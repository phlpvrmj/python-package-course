from setuptools import setup, find_packages

setup(
    name="toast_calculator",
    version="1.0",
    description="A fancy function to calculate stuff",
    author="Philip",
    packages=find_packages()
)