# Multiply input
def multiply(x: int, y: float) -> float:
    """
    Fancy calculation function

    Args:
        x (int): first number
        y (float):  second number

    Returns:
        float: multiplies x and y
    """
    return x * y

# Amount of toasts
def calculate_toast(n: int) -> int:
    """
    Calculate amount of toasts for given number of people

    :param n: amount of people
    :return: amount of toasts
    """
    toast = ((n - 1) * n) / 2
    return toast